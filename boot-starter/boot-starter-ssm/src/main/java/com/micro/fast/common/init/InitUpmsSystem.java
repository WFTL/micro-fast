package com.micro.fast.common.init;

import com.micro.fast.common.init.properties.SqlInitProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

/**
 * 单一数据源时初始化数据库表结构和数据
 * 如果配置多个数据源，按此自行添加
 * @author lsy
 */
@Component
@Slf4j
public class InitUpmsSystem implements ApplicationListener<ContextRefreshedEvent> {
    public static final String ANNOTATION_CONFIG_EMBEDDED_WEB_APPLICATION_CONTEXT = "AnnotationConfigEmbeddedWebApplicationContext";
    public static final String ANNOTATION_CONFIG_APPLICATION_CONTEXT = "AnnotationConfigApplicationContext";

    @Autowired
    private Map<String,SqlInitProperties> sqlInitPropertiesMap;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        ApplicationContext parent = applicationContext.getParent();
        // 保证springBoot上下文初始化完毕
        if (applicationContext.getDisplayName().contains(ANNOTATION_CONFIG_EMBEDDED_WEB_APPLICATION_CONTEXT)
                && parent.getDisplayName().contains(ANNOTATION_CONFIG_APPLICATION_CONTEXT)) {
            // 初始化数据表
            sqlInitPropertiesMap.forEach((key,value) -> {
                if (value != null) {
                    runInitSql(value.getDriverClassName(),value.getUrl(),value.getUsername(),value.getPassword(),value.getSqlPath());
                }
            });
            log.info("name:{} displayName: {} parentName:{} parentName: {}",
                    applicationContext.getApplicationName(),
                    applicationContext.getDisplayName(),
                    parent.getApplicationName(),
                    parent.getDisplayName());
        }
    }

    /**
     * 执行初始化sql
     * @param driver 数据库驱动全类名
     * @param url 数据库连接url
     * @param username 数据库连接的用户名
     * @param password 数据库连接的密码
     * @param sqlPath sql脚本的路径
     */
    private void runInitSql(String driver,String url,String username,String password,String sqlPath){
        try {
            // 执行sql脚本
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url, username, password);
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            Resources.setCharset(Charset.forName("utf-8"));
            Reader resourceAsReader = Resources.getResourceAsReader(sqlPath);
            scriptRunner.runScript(resourceAsReader);
            scriptRunner.closeConnection();
            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
